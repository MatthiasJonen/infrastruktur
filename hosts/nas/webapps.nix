{ config, pkgs, ... }:

{
  # UptimeKuma
  services.uptime-kuma.enable = true;
}
