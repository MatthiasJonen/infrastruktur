{ config, pkgs, ... }:

{
  # Relevante Packages installieren
  environment.systemPackages = with pkgs; [
    btrfs-progs
    dave
  ];
  # Ports in Firewall öffnen
  networking.firewall.allowedTCPPorts = [
    8000 #dave
    9000 #minio
    9001 #minio
  ];

  # btrfs subvolumes mounten
  fileSystems."/mnt/faststorage" = {
    device = "/dev/nvme0n1";
    fsType = "btrfs";
    options = [ "subvolid=261" "compress=zstd:3" "autodefrag" "noatime" "commit=300" "space_cache=v2" ];
  };

  fileSystems."/mnt/faststorage/applicationdata" = {
    device = "/dev/nvme0n1";
    fsType = "btrfs";
    options = [ "subvolid=256" "compress=zstd:3" "autodefrag" "noatime" "commit=300" "space_cache=v2" ];
  };

  fileSystems."/mnt/faststorage/snapshots" = {
    device = "/dev/nvme0n1";
    fsType = "btrfs";
    options = [ "subvolid=257" "compress=zstd:3" "autodefrag" "noatime" "commit=300" "space_cache=v2" ];
  };

  fileSystems."/mnt/faststorage/share" = {
    device = "/dev/nvme0n1";
    fsType = "btrfs";
    options = [ "subvolid=272" "compress=zstd:3" "autodefrag" "noatime" "commit=300" "space_cache=v2" ];
  };

  # automatische Snapshots erstellen
  services.btrbk.instances."applicationdata" = {
    onCalendar = "daily";
    settings = {
      snapshot_preserve_min = "2d";
      snapshot_preserve = "30d";
      volume."/mnt/faststorage" = {
        subvolume = "applicationdata";
	snapshot_dir = "/mnt/faststorage/snapshots/applicationdata";
      };
    };
  };
  services.btrbk.instances."share" = {
    onCalendar = "daily";
    settings = {
      snapshot_preserve_min = "2d";
      snapshot_preserve = "30d";
      volume."/mnt/faststorage" = {
        subvolume = "share";
	snapshot_dir = "/mnt/faststorage/snapshots/share";
      };
    };
  };
  # Lokales WEBDAV mittels dave
  age.secrets.config-webdav-dave = {
    file = ../../secrets/config-webdav-dave.age;
    path = "/config/config.yaml";
    mode = "600";
    owner = "root";
    group = "root";
    symlink = false;
  };
  systemd.services.webdav-dave = {
    description = "Dave service starten und laufen lassen";
    wantedBy = [ "multi-user.target" ];
    serviceConfig.Type = "simple";
    serviceConfig.WorkingDirektory = "/etc/dave";
    serviceConfig.ExecStart = "/run/current-system/sw/bin/dave";
  };

  # Minio S3 Storage
  fileSystems."/mnt/minio/ORICO1" = {
    device = "/dev/sdb";
    fsType = "xfs";
    options = [ "defaults" "noatime" ];
  };
  fileSystems."/mnt/minio/ORICO2" = {
    device = "/dev/sdc";
    fsType = "xfs";
    options = [ "defaults" "noatime" ];
  };
  fileSystems."/mnt/minio/ORICO3" = {
    device = "/dev/sdd";
    fsType = "xfs";
    options = [ "defaults" "noatime" ];
  };
  fileSystems."/mnt/minio/ORICO4" = {
    device = "/dev/sde";
    fsType = "xfs";
    options = [ "defaults" "noatime" ];
  };
  fileSystems."/mnt/minio/ORICO5" = {
    device = "/dev/sdf";
    fsType = "xfs";
    options = [ "defaults" "noatime" ];
  };
  age.secrets.minio.file = ../../secrets/minio-rootCredentialsFile.age;
  services.minio = {
    enable = true;
    configDir = "/mnt/faststorage/applicationdata/minio";
    dataDir = [
      "/mnt/minio/ORICO1"
      "/mnt/minio/ORICO2"
      "/mnt/minio/ORICO3"
      "/mnt/minio/ORICO4"
      "/mnt/minio/ORICO5"
    ];
    rootCredentialsFile = "/run/agenix/minio";
  };
}
