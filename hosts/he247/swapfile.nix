{ config, pkgs, ... }:

{
  # Swapfile generieren und mounten. Bei Hetzner ist RAM teuer aber die SSDs schnell
  swapDevices = [{
    device = "/var/lib/swapfile";
    size = 6*1024;
  }];
}
