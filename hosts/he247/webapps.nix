{ config, pkgs, ... }:

{
  # Notifications mit ntfy
  services.ntfy-sh = {
    enable = true;
    settings = {
      base-url = "http://ntfy.jonen.at";
      listen-http = ":2586";
      behind-proxy = true;
      upstream-base-url = "https://ntfy.sh";
    };
  };
  services.caddy.virtualHosts."ntfy.jonen.at".extraConfig = ''
    reverse_proxy :2586

    # Redirect HTTP to HTTPS, but only for GET topic addresses, since we want
    # it to work with curl without the annoying https:// prefix
    @httpget {
        protocol http
        method GET
        path_regexp ^/([-_a-z0-9]{0,64}$|docs/|static/)
    }
    redir @httpget https://{host}{uri}
  '';

  # Nextcloud
  ## User www-data anlegen
  users.groups.www-data = {};
  users.users.www-data = {
    name = "www-data";
    uid = 33;
    group = "www-data";
    isSystemUser = true;
  };
  ## Systemd Service zum Container starten
  systemd.services.nextcloud = {
    description = "Nextcloud AIO per Docker starten";
    requires = [ "docker.service" ];
    after = [ "docker.service" ];
    serviceConfig.Type = "oneshot";
    script = with pkgs; ''
      # Definiere Variablen
      nextcloud_data=/mnt/storagebox_u333248/nextcloud_data
      docker_bin=/run/current-system/sw/bin/docker
      occ="$docker_bin exec --user www-data nextcloud-aio-nextcloud php occ"
      neustart_notwendig=false

      # Erstelle DatenVerzeichnis wenn es nicht existiert
      mkdir -p $nextcloud_data
      chmod -R 0750 $nextcloud_data
      chown -R www-data:root $nextcloud_data

      # Docker run command
      # Nur starten, wenn noch nicht gestartet
      if ! $($docker_bin ps --format "table {{.ID}}\t{{.Names}}" | grep -qw "nextcloud-aio-mastercontainer"); then
        $docker_bin run -d \
        --sig-proxy=false \
        --name nextcloud-aio-mastercontainer \
        --restart always \
        --publish 8080:8080 \
        -e APACHE_PORT=11000 \
        -e APACHE_IP_BINDING=127.0.0.1 \
        -e NEXTCLOUD_DATADIR="$nextcloud_data" \
        --volume nextcloud_aio_mastercontainer:/mnt/docker-aio-config \
        --volume /var/run/docker.sock:/var/run/docker.sock:ro \
        nextcloud/all-in-one:latest;
      fi

      # Einstellungen vornehmen
      if [ "$($occ config:system:get default_phone_region)" != "AT" ]; then
        $occ config:system:set default_phone_region --value="AT"
	neustart_notwendig=true
      fi

      # Zusätzliche Apps installieren
      installierte_apps="$($occ app:list --output=json_pretty)"
      zu_installierende_apps="news forms groupfolders"
      zu_aktivierende_apps="bruteforcesettings files_external suspicious_login"

      for app in $zu_installierende_apps; do
        if ! $(echo $installierte_apps | grep -qw "\"$app\":"); then
          $occ -n app:install $app
        fi
      done

      for app in $zu_aktivierende_apps; do
        if $(echo $installierte_apps | grep -qw "\"$app\": null"); then
          $occ -n app:enable $app
        fi
      done

      # Apps updaten
      $occ -n app:update --all

      # wenn notwendig nextcloud neu starten um alle Einstellungen zu übernehmen
      if [ $neustart_notwendig ]; then 
        $docker_bin restart nextcloud-aio-mastercontainer
      fi
    '';
  };

  ## Nextcloud - Standard Seite
  networking.firewall.allowedTCPPorts = [ 3478 ];
  networking.firewall.allowedUDPPorts = [ 3478 ];
  services.caddy.virtualHosts."cloud.jonen.at".extraConfig = ''
    header Strict-Transport-Security max-age=31536000;
    reverse_proxy localhost:11000
  '';

  ## Nextcloud fail2ban Ergaenzung
  environment.etc = {
    nextcloud = {
      target = "/fail2ban/filter.d/nextcloud.conf";
      text = ''
        [Definition]
        _groupsre = (?:(?:,?\s*"\w+":(?:"[^"]+"|\w+))*)
        failregex = ^\{%(_groupsre)s,?\s*"remoteAddr":"<HOST>"%(_groupsre)s,?\s*"message":"Login failed:
                    ^\{%(_groupsre)s,?\s*"remoteAddr":"<HOST>"%(_groupsre)s,?\s*"message":"Trusted domain error.
        datepattern = ,?\s*"time"\s*:\s*"%%Y-%%m-%%d[T ]%%H:%%M:%%S(%%z)?"
      '';
    };
  };
  services.fail2ban.jails = {
    nextcloud = ''
      backend = auto
      enabled = true
      port = 80,443,8080,8443,3478
      protocol = tcp
      filter = nextcloud
      maxretry = 3
      bantime = 86400
      findtime = 43200
      logpath = /var/lib/docker/volumes/nextcloud_aio_nextcloud/_data/data/nextcloud.log
      chain = DOCKER-USER
    '';
  };
  ## Nextcloud - Upload über lokales Netzwerk zuhause ermöglichen
  environment.systemPackages = [
    (pkgs.writeShellScriptBin "nextcloudupload" ''
    #Daten von nas zuhause in nextcloud laden
    echo "nextcloudupload gestartet."
    
    while true
    do
    # Variablen definieren
    users="matthias manuela"
      for user in $users; do
        # Variablen definieren
        ziel="/mnt/storagebox_u333248/nextcloud_data/$user/files/Upload/"
        quelle="/mnt/faststorage/share/webdav/$user/"
      
        # Sicherstellen, dass Zielverzeichnis existiert
        mkdir -p $ziel 
      
        # nur uploaden, wenn es Daten vorhanden sind
        if [ "$(/run/current-system/sw/bin/ssh root@nas "ls -A $quelle")" ]; then
          # als temporäre Sicherung btrfs Snapshot der Daten erstellen
          ssh root@nas "/run/current-system/sw/bin/btrbk -c /etc/btrbk/share.conf run"
          # Daten uploaden
          /run/current-system/sw/bin/rsync -az --remove-source-files --chown=www-data:root root@nas:$quelle $ziel
          # Daten in Nextcloud sichtbar machen
          /run/current-system/sw/bin/docker exec --user www-data nextcloud-aio-nextcloud php occ files:scan -n --path="$user/files/Upload"
        fi
      done
      sleep 5m
    done
    '')
  ]; 
  systemd.services.nextcloudupload = {
    description = "Nextcloudupload script starten und laufen lassen";
    wantedBy = [ "multi-user.target" ];
    path = with pkgs; [ openssh ];
    serviceConfig.Type = "simple";
    serviceConfig.ExecStart = "/run/current-system/sw/bin/nextcloudupload";
  };
  # Nextcloud-aio-mastercontainer neustarten. Immer mal wieder nötig, ich weiß noch nicht, warum.
  systemd.services.nextcloud-aio-mastercontainer-restart= {
    description = "Nextcloud-aio-mastercontainer neustarten. Immer mal wieder nötig, weiß noch nicht warum.";
    after = [ "network-online.target" ];
    wants = [ "network-online.target" ];
    startAt = "00:55";
    path = [
      pkgs.docker
      ];
    script = with pkgs; ''
    ${docker}/bin/docker restart nextcloud-aio-mastercontainer
    '';
  };
}
