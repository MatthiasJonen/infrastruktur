{ config, pkgs, ... }:

{
  # Hetzner Storage box mounten 
  age.secrets.smbsecrets-hetzner-storagebox_u333248.file = ../../secrets/smbsecrets-hetzner-storagebox_u333248.age;
  environment.systemPackages = [ pkgs.cifs-utils ];
  fileSystems."/mnt/storagebox_u333248" = {
    device = "//u333248.your-storagebox.de/backup";
    fsType = "cifs";
      options = let
        # this line prevents hanging on network split
        automount_opts = "seal,x-systemd.automount,noauto,x-systemd.idle-timeout=60,x-systemd.device-timeout=5s,x-systemd.mount-timeout=5s";

      in ["${automount_opts},credentials=/run/agenix/smbsecrets-hetzner-storagebox_u333248,uid=33,gid=0,file_mode=0770,dir_mode=0770"];
  };

  # synology backup mounten
  fileSystems."/mnt/backup" = {
    device = "100.85.234.8:/volume1/NetBackup";
    fsType = "nfs";
    options = [ "x-systemd.automount" "noauto" ];
  };
}
