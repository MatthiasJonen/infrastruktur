{ config, pkgs, ... }:

{
  # Reverse proxy mit caddyserver
  networking.firewall.allowedTCPPorts = [ 80 443 ];
  services.caddy = {
    enable = true;
    virtualHosts."uptime.jonen.at".extraConfig = ''
      reverse_proxy http://nas:3001
    '';
  };
}
