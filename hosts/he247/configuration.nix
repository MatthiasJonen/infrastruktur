{ ... }: {
  imports = [
    ./hardware-configuration.nix
    ./storage.nix
    ./swapfile.nix
    # Profiles
    ../../profiles/server.nix
    # Lokale Applikationen
    ./reverseproxy.nix
    ./webapps.nix
  ];

  boot.cleanTmpDir = true;
  zramSwap.enable = true;
  networking.hostName = "he247";
  networking.domain = "";
  services.openssh.enable = true;
  users.users.root.openssh.authorizedKeys.keys = [
    "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIDY7MouGsBcsYvZNJ2LXmny0oirM4VKwl0NFWYhmHlOW matthias@jonen.at"
  ];
}
