#!/bin/sh -v

if [[ ! -L /etc/nixos/configuration.nix ]]
then
    sudo mv /etc/nixos/configuration.nix /etc/nixos/configuration.nix.bac
    sudo mv /etc/nixos/hardware-configuration.nix /etc/nixos/hardware-configuration.nix.bac
fi

sudo ln -sf $PWD/hosts/$HOSTNAME/configuration.nix /etc/nixos/configuration.nix