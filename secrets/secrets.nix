let
  general = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIGCNcl/VtT6K0oY4RWt0OqO56Tw+H+3jou5OXJNhERf4";
  agenixdev = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIFEFHVE5OcpauSbGlxCLcmZDUmPm6292vDoyIFaBoaKm";
in
{
  "passwordfile-matthias.age".publicKeys = [ general agenixdev ];
  "passwordfile-manuela.age".publicKeys = [ general agenixdev ];
  "authkey-tailscale.age".publicKeys = [ general agenixdev ];
  "config-ssh-matthias.age".publicKeys = [ general agenixdev ];
  "privatekey-agenixdev-matthias.age".publicKeys = [ general agenixdev ];
  "privatekey-gitlab-matthias.age".publicKeys = [ general agenixdev ];
  "privatekey-github-matthias.age".publicKeys = [ general agenixdev ];
  "privatekey-homelab-matthias.age".publicKeys = [ general agenixdev ];
  "privatekey-hetzner.age".publicKeys = [ general agenixdev ];
  "smbsecrets-hetzner-storagebox_u333248.age".publicKeys = [ general agenixdev ];
  "config-webdav-dave.age".publicKeys = [ general agenixdev ];
  "minio-rootCredentialsFile.age".publicKeys = [ general agenixdev ];
  "minio-accesskey.age".publicKeys = [ general agenixdev ];
}
