{ config, lib, pkgs, ... }:

{
  imports = [
    ../modules/core
    ../modules/user/matthias.nix
    ../modules/user/manuela.nix
    ../modules/workstation
  ];
}