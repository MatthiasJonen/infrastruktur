{ config, pkgs, ... }:

{
  age.secrets.privatekey-gitlab-matthias = {
    file = ../../secrets/privatekey-gitlab-matthias.age;
    path = "/home/matthias/.ssh/gitlab";
    mode = "600";
    owner = "matthias";
    group = "users";
    symlink = false;
  };
}