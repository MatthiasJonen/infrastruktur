{ config, pkgs, ... }:

{
  age.secrets.privatekey-github-matthias = {
    file = ../../secrets/privatekey-github-matthias.age;
    path = "/home/matthias/.ssh/github";
    mode = "600";
    owner = "matthias";
    group = "users";
    symlink = false;
  };
}