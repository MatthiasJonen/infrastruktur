{ config, lib, pkgs, ... }:

{
  imports = [
    ./privatekey-agenixdev.nix
    ./privatekey-gitlab.nix
    ./privatekey-github.nix
    ./privatekey-hetzner.nix
    ./privatekey-homelab.nix
  ];

  age.secrets.passwordfile-matthias.file = ../../secrets/passwordfile-matthias.age;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.matthias = {
    isNormalUser = true;
    passwordFile = config.age.secrets.passwordfile-matthias.path;
    description = "Matthias Jonen";
    extraGroups = [ "networkmanager" "wheel" ];
    shell = pkgs.fish;
    openssh.authorizedKeys.keys = [
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIFFdPOQhbsXv3b6LcIHzrL3ElKT5jqC8hTKztCQnsxLf matthias@homelab"
    ];
    #packages = with pkgs; [];
  };

  # ssh config laden
  age.secrets.config-ssh-matthias = {
    file = ../../secrets/config-ssh-matthias.age;
    path = "/home/matthias/.ssh/config";
    mode = "600";
    owner = "matthias";
    group = "users";
    symlink = false;
  };
}
