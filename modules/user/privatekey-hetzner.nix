{ config, pkgs, ... }:

{
  age.secrets.privatekey-hetzner = {
    file = ../../secrets/privatekey-hetzner.age;
    path = "/home/matthias/.ssh/hetzner";
    mode = "600";
    owner = "matthias";
    group = "users";
    symlink = false;
  };
}