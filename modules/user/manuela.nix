{ config, lib, pkgs, ... }:

{
  imports = [
  ];

  age.secrets.passwordfile-manuela.file = ../../secrets/passwordfile-manuela.age;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.manuela = {
    isNormalUser = true;
    passwordFile = config.age.secrets.passwordfile-manuela.path;
    description = "Manuela Jonen";
    extraGroups = [ "networkmanager" ];
    shell = pkgs.fish;
  };
}