{ config, pkgs, ... }:

{
  age.secrets.privatekey-agenixdev-matthias = {
    file = ../../secrets/privatekey-agenixdev-matthias.age;
    path = "/home/matthias/.ssh/agenixdev";
    mode = "600";
    owner = "matthias";
    group = "users";
    symlink = false;
  };
}