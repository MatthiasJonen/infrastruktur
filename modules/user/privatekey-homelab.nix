{ config, pkgs, ... }:

{
  age.secrets.privatekey-homelab-matthias = {
    file = ../../secrets/privatekey-homelab-matthias.age;
    path = "/home/matthias/.ssh/homelab";
    mode = "600";
    owner = "matthias";
    group = "users";
    symlink = false;
  };
}