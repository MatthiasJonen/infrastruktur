{ config, lib, pkgs, ... }:

{
  imports = [
    ./displaylink.nix
  ];

  # Enable touchpad support (enabled default in most desktopManager).
  services.xserver.libinput.enable = true;
}