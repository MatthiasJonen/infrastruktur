{ config, lib, pkgs, ... }:

{
  # Firewall für Tailscale Netzwerk öffnen
  networking.firewall.trustedInterfaces = [ "tailscale0" ];
  networking.firewall.allowedUDPPorts = [ config.services.tailscale.port ];
}
