{ config, lib, pkgs, ... }:

{
  imports = [
    ./openssh.nix
    ./networking.nix
  ];

}
