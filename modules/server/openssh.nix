{ config, lib, pkgs, ... }:

{
  # Enable the OpenSSH daemon.
  services.openssh = {
    enable = true;
    openFirewall = true;
    permitRootLogin = "no";
    passwordAuthentication = false;
    logLevel = "VERBOSE";
  };

  # Configure tailscale ssh access
  systemd.services.tailscale-advertise-ssh = {
    description = "Advertise tailscale ssh on host";

    # make sure tailscale is running before trying to connect to tailscale
    after = [ "network-pre.target" "tailscaled.service" ];
    wants = [ "network-pre.target" "tailscaled.service" ];
    wantedBy = [ "multi-user.target" ];

    # set this service as a oneshot job
    serviceConfig.Type = "oneshot";

    # have the job run this shell script
    script = with pkgs; ''
      # wait for tailscaled to settle
      sleep 2

      # advertise tailscale ssh
      ${tailscale}/bin/tailscale up --ssh
    '';
  };
}
