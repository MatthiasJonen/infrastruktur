{ config, pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    gum
    (writeShellScriptBin "infrastruktur-management" ''
    echo "aktive NixOS Konfiguration:"
    ls -l /etc/nixos/configuration.nix

    echo "NixOS Konfiguration aktualisieren:"
    befehl=$(gum choose --limit=1 --selected=test test switch)
    if [ $befehl = 'test' ]; then
      sudo nixos-rebuild test --fast
    elif [ $befehl = 'switch' ]; then
      sudo nixos-rebuild switch
    fi
    '')
  ];
}
