{ config, lib, pkgs, ... }:

with lib;

let
  sysconfig = (import <nixpkgs/nixos> {}).config;
  home-manager = builtins.fetchTarball "https://github.com/nix-community/home-manager/archive/release-22.11.tar.gz";
in {
  imports = [
    (import "${home-manager}/nixos")
  ];

  config = mkMerge [{
    # Alle Hosts
    home-manager.users.matthias = import ../../home-manager/user/matthias/default.nix;
    }( mkIf (sysconfig.services.xserver.enable){
    # Desktop Hosts
    home-manager.users.manuela = import ../../home-manager/user/manuela/default.nix;
    })
  ];
}
