{ config, lib, pkgs, ... }:

# Services which optimise NixOS, the host hardware. Should lead to minimal manual maintainance.

{
  # Garbage Collection: delete old, unused packages
  nix.gc = {
    automatic = true;
    dates = "weekly";
    options = "--delete-older-than 10d";
    persistent = true;
    randomizedDelaySec = "7min";
  };

  # Automatic upgrades
  system.autoUpgrade = {
    enable = true;
    dates = "00:15";
    allowReboot = false;
    randomizedDelaySec = "30min";
    persistent = true;
    channel = https://nixos.org/channels/nixos-22.11;
  };

  # Storage optimization
  nix.settings.auto-optimise-store = true;

  # SSD optimazation: periodic SSD TRIM of mounted partitions in background
  services.fstrim = {
    enable = true;
    interval = "monthly";
  };

  # CPU performance optimazation
  services.irqbalance.enable = true;

  # Firmware updates ermöglichen
  services.fwupd = {
    enable = true;
  };

  # Automatisches Deployment mit ansible-pull wenn sich auf der Main Branch was ändert.
  systemd.services.deployment = {
    description = "Automatisches Deployment der aktuellen nixos Konfiguration, wenn sich auf der main Branch etwas ändert.";
    after = [ "network-online.target" ];
    wants = [ "network-online.target" ];
    startAt = "*:0/5";
    path = [
      pkgs.git
      pkgs.nixos-rebuild
      pkgs.nix
      ];
    script = with pkgs; ''
    export NIX_PATH=/root/.nix-defexpr/channels:nixpkgs=/nix/var/nix/profiles/per-user/root/channels/nixos:nixos-config=/etc/nixos/configuration.nix:/nix/var/nix/profiles/per-user/root/channels
    ${ansible}/bin/ansible-pull --connection=local --inventory 127.0.0.1, --limit 127.0.0.1 --checkout main --url https://gitlab.com/MatthiasJonen/infrastruktur.git --only-if-changed /root/infrastruktur/ansible/deployment.yaml
    '';
  };

  # Automatisches Deployment mit ansible-pull unabhängig von Veränderungen am Repo
  systemd.services.deployment-ohne-Commit = {
    description = "Automatisches Deployment der aktuellen nixos Konfiguration, unabhängig von Veränderungen am Repo.";
    after = [ "network-online.target" ];
    wants = [ "network-online.target" ];
    startAt = "*-*-* 06:01:00";
    path = [
      pkgs.git
      pkgs.nixos-rebuild
      pkgs.nix
      ];
    script = with pkgs; ''
    export NIX_PATH=/root/.nix-defexpr/channels:nixpkgs=/nix/var/nix/profiles/per-user/root/channels/nixos:nixos-config=/etc/nixos/configuration.nix:/nix/var/nix/profiles/per-user/root/channels
    ${ansible}/bin/ansible-pull --connection=local --inventory 127.0.0.1, --limit 127.0.0.1 --checkout main --url https://gitlab.com/MatthiasJonen/infrastruktur.git /root/infrastruktur/ansible/deployment.yaml
    '';
  };
}
