{ config, pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    arion
    docker-client
  ];

  # Arion works with Docker, but for NixOS-based containers, you need Podman
  # since NixOS 21.05.
  virtualisation.docker.enable = true;
  virtualisation.podman.enable = false;
  virtualisation.podman.dockerSocket.enable = false;
  virtualisation.podman.defaultNetwork.dnsname.enable = false;

  users.extraUsers.matthias.extraGroups = ["podman" "docker"];
}