{ config, pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    glances
  ];

  systemd.services.glances = {
    description = "Glances im Server Modus starten und laufen lassen";
    wantedBy = [ "multi-user.target" ];
    serviceConfig.Type = "simple";
    serviceConfig.ExecStart = "/run/current-system/sw/bin/glances -s";
  };
}
