{ config, lib, pkgs, ... }:

{
  # Allow unfree packages
  nixpkgs.config.allowUnfree = true;

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    neovim # Do not forget to add an editor to edit configuration.nix! The Nano editor is also installed by default.
    wget
    git
    fzf
    fd
    bat
    pciutils
    usbutils
    lsof # Check open ports mit sudo lsof -i -P -n | grep LISTEN
    ansible # Deployment
    python3Full
    minio-client
    s3fs # um s3 buckets mounten zu können
  ];
}
