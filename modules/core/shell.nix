{ config, lib, pkgs, ... }:

{
  # Enable shells
  programs.fish = {
    enable = true;
    shellInit = "
      source /var/run/current-system/sw/share/fish/vendor_completions.d/zellij.fish
    ";
    interactiveShellInit = "
      set fish_greeting \"\"
      set -gx EDITOR /home/matthias/.nix-profile/bin/hx
    ";
    shellAbbrs = {
      nix-switch = "sudo nixos-rebuild switch";
      nix-test = "sudo nixos-rebuild test";
      nix-drybuild = "sudo nixos-rebuild dry-build -v";
      occ = "docker exec --user www-data nextcloud-aio-nextcloud php occ";
    };
  };
  environment.systemPackages = with pkgs; [
    meslo-lgs-nf         # Nerd font damit Symbole im Terminal angezeigt werden
    fishPlugins.tide     # The ultimate fish prompt
    fishPlugins.done     # notifier bei langen commands
    fishPlugins.pisces   # automatisch klammern schließen
    fishPlugins.fzf-fish # Ctrl+Alt+F files suchen, Ctrl+R history suchen
    fishPlugins.colored-man-pages
    fishPlugins.sponge   # History automatisch von typos befreien
    screen # Sessions im Terminal
    tmux   # Sessions im Terminal
    zellij # Terminal workspace
    lazygit # Nettes TUI für git
    ripgrep # Search utility
    ripgrep-all # Search utilty
    xclip   # X session system clipboard
    wl-clipboard # Wayland system clipboard
  ];

  # Setup neovim
  programs.neovim = {
    enable = true;
    defaultEditor = true;
  };
}
