{ config, pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    ifuse # iphone speicher erreichbar machen
  ];
  services.usbmuxd.enable = true; # iphone speicher erreichbar machen

  users.extraUsers.matthias.extraGroups = ["fuse"];
}