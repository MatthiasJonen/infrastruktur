{ config, pkgs, ... }:

{
  # Packages for workstation
  environment.systemPackages = with pkgs; [
    bitwarden
    appimage-run
    nextcloud-client
    glib-networking # löst einen tls fehler beim adden eines Flatpak repos
  ];

  # Services for workstation

  # Flatpaks, hauptsächlich von offiziellen Publishern
  systemd.services.installorupdate-flatpaks = {
    description = "Flatpaks installieren oder updaten";
    after = [ "network-online.target" ];
    serviceConfig.Type = "oneshot";
    script = with pkgs; ''
    ${flatpak}/bin/flatpak install --or-update --noninteractive flathub ch.protonmail.protonmail-bridge
    ${flatpak}/bin/flatpak install --or-update --noninteractive flathub org.signal.Signal
    ${flatpak}/bin/flatpak install --or-update --noninteractive flathub org.mozilla.firefox
    ${flatpak}/bin/flatpak install --or-update --noninteractive flathub com.usebottles.bottles
    ${flatpak}/bin/flatpak install --or-update --noninteractive flathub com.github.tchx84.Flatseal
    '';
  };
}