{ config, pkgs, ... }:

{
  # Enable the GNOME Desktop Environment.
  services.xserver.displayManager.gdm = {
    enable = true;
    wayland = true;
  };
  services.xserver.desktopManager.gnome.enable = true;

  # Gnome extensions and applications
  environment.systemPackages = with pkgs; [
    gnomeExtensions.tray-icons-reloaded
    gnomeExtensions.just-perfection
    gnomeExtensions.dash-to-dock
    pkgs.gnome.gnome-tweaks
    gnome.geary # Email
    gnome.dconf-editor
    guake # dropdown terminal #todo: konfigurieren und in autostart nehmen
  ];
}