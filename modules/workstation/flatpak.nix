{ config, pkgs, ... }:

{
  #Flatpak aktivieren
  services.flatpak.enable = true;
  #xdg.portal.extraPortals = [ pkgs.xdg-desktop-portal-gtk ]; #nur wenn nicht gnome installiert ist

  #Flatpak repo ergänzen
  systemd.services.flatpak-repo = {
    description = "Flathub Repo für Flatpak ergänzen";
    after = [ "network-online.target" ];
    wants = [ "network-online.target" ];
    serviceConfig.Type = "oneshot";
    script = with pkgs; ''
    ${flatpak}/bin/flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
    ${flatpak}/bin/flatpak update --noninteractive
    '';
  };
}