{ config, pkgs, ... }:

{
  environment.systemPackages = [
    pkgs.libreoffice
    pkgs.corefonts
  ];
}