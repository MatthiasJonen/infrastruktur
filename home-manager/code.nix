{ config, lib, pkgs, ... }:

with lib;

let 
  sysconfig = (import <nixpkgs/nixos> { }).config;
in {
  config = mkMerge [
    {
      # Packages
      home.packages = with pkgs; [
        # LSP
        rnix-lsp # nix
        nixfmt # nix
        nodePackages.bash-language-server # bash
        nodePackages.dockerfile-language-server-nodejs # Dockerfile
        nodePackages.vscode-langservers-extracted # hauptsächlich json
        #marksman # markdown
        taplo # TOML
        nodePackages.yaml-language-server # yaml
        # CLI Tools für Entwicklung
        manix # Nix Dokumentation
        nixfmt # Nix Formater
      ];

      # git
      programs.git = {
        enable = true;
        package = pkgs.gitAndTools.gitFull;
        userEmail = "accounts.matthias@jonen.at";
        userName = "MatthiasJonen";
      };

      # Helix text editor
      programs.helix = {
        enable = true;
        settings = {
          theme = "base16";
          editor = {
            line-number = "relative";
            # bufferline = "always";
            color-modes = true;
            lsp.display-messages = true;
            indent-guides.render = true;
            cursor-shape = {
              insert = "bar";
              normal = "block";
              select = "underline";
            };
          };
          keys.normal = {
            esc = [ "collapse_selection" "keep_primary_selection" ];
          };
        };
      };
      # neovim
      # https://github.com/ThePrimeagen/init.lua
      # https://github.com/notusknot/dotfiles-nix/blob/main/modules/nvim/default.nix
      programs.neovim = {
        enable = true;
        package = pkgs.neovim-unwrapped;
        extraLuaConfig = ''
          -- Set
          vim.opt.guicursor = ""

          vim.opt.nu = true
          vim.opt.relativenumber = true

          vim.opt.tabstop = 4
          vim.opt.softtabstop = 4
          vim.opt.shiftwidth = 4
          vim.opt.expandtab = true

          vim.opt.smartindent = true

          vim.opt.wrap = false

          vim.opt.swapfile = false
          vim.opt.backup = false
          vim.opt.undodir = os.getenv("HOME") .. "/.vim/undodir"
          vim.opt.undofile = true
          vim.opt.clipboard = "unnamedplus"

          vim.opt.hlsearch = false
          vim.opt.incsearch = true

          vim.opt.termguicolors = true

          vim.opt.scrolloff = 8
          vim.opt.signcolumn = "yes"
          vim.opt.isfname:append("@-@")

          vim.opt.updatetime = 50
              
          vim.opt.colorcolumn = "120"
          vim.completeopt = "menuone,noselect"

          -- remap keymapping
          vim.g.mapleader = " "
          vim.keymap.set("n", "<leader>pv", vim.cmd.Ex)

          -- plugin config
          -- telescope
          local builtin = require('telescope.builtin')
          vim.keymap.set('n', '<leader>ff', builtin.find_files, {})
          vim.keymap.set('n', '<leader>pf', builtin.git_files, {})
          vim.keymap.set('n', '<leader>ps', function()
              builtin.grep_string({ search = vim.fn.input("Grep > ") })
          end)
          vim.keymap.set('n', '<leader>vh', builtin.help_tags, {})

          -- harpoon file switching
          local mark = require("harpoon.mark")
          local ui = require("harpoon.ui")

          vim.keymap.set("n", "<leader>a", mark.add_file)
          vim.keymap.set("n", "<leader>e", ui.toggle_quick_menu)

          vim.keymap.set("n", "<leader>1", function() ui.nav_file(1) end)
          vim.keymap.set("n", "<leader>2", function() ui.nav_file(2) end)
          vim.keymap.set("n", "<leader>3", function() ui.nav_file(3) end)
          vim.keymap.set("n", "<leader>4", function() ui.nav_file(4) end)

          -- colorscheme
          require('rose-pine').setup({
                  disable_background = true
          })
          function ColorMyPencils(color) 
              color = color or "rose-pine"
              vim.cmd.colorscheme(color)

              vim.api.nvim_set_hl(0, "Normal", { bg = "none" })
              vim.api.nvim_set_hl(0, "NormalFloat", { bg = "none" })
          end
          ColorMyPencils()

          --Undotree
          vim.keymap.set("n", "<leader>u",vim.cmd.UndotreeToggle)
        '';
        plugins = with pkgs.vimPlugins; [
          # Fuzzy finding, file switcher und code highlighting
          plenary-nvim
          telescope-nvim
          telescope-fzf-native-nvim
          harpoon
          {
            plugin = nvim-treesitter.withAllGrammars;
            config = ''
              lua << EOF
              require('nvim-treesitter.configs').setup {
                  highlight = {
                      enable = true,
                      additional_vim_regex_highlighting = false,
                  },
              }
              EOF
            '';
          }
          # Colorscheme
          rose-pine
          # Versionierung von Files
          undotree
          # LSP 
          {
            plugin = nvim-lspconfig;
            config = ''
              lua <<EOF
              require('lspconfig').rnix.setup{}
              EOF
            '';
          }
        ];
      };
    }
    (mkIf (sysconfig.services.xserver.enable) {
      # vscode
      programs.vscode = {
        enable = true;
        package = pkgs.vscodium;
        extensions = with pkgs.vscode-extensions; [
          bbenoist.nix
          gruntfuggly.todo-tree
          jnoortheen.nix-ide
        ];
        userSettings = {
          "update.mode" = "none";
          "files.trimTrailingWhitespace" = true;
          "[nix]"."editor.tabSize" = 2;
          "nix.enableLanguageServer" = true;
          "git.autofetch" = true;
          "git.confirmSync" = false;
          "explorer.confirmDelete" = false;
        };
      };
      home.packages = [
        pkgs.rnix-lsp
        pkgs.ansible-lint # TODO: in Repo integrieren
        pkgs.ansible-doctor # TODO: in Repo integrieren, ich denke das wird ein pre-commit
      ];
    })
  ];
}
