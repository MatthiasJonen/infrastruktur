{ config, lib, pkgs, ... }:

with lib;

let
  sysconfig = (import <nixpkgs/nixos> {}).config;
in {
  config = mkIf (sysconfig.services.xserver.enable){
    home.packages = [
      pkgs.rssguard
    ];
    # dconf dump / > ~/Downloads/dconf.dump hilft bei Kontrolle der dconf Einstellungen
    dconf.settings = {
      "org/gnome/shell" = {
        enabled-extensions = [
          "trayIconsReloaded@selfmade.pl"
          "workspace-indicator@gnome-shell-extensions.gcampax.github.com"
          "auto-move-windows@gnome-shell-extensions.gcampax.github.com"
          "places-menu@gnome-shell-extensions.gcampax.github.com"
          "apps-menu@gnome-shell-extensions.gcampax.github.com"
          ];
        favorite-apps = [
          "org.mozilla.firefox.desktop"
          "codium.desktop"
          "org.gnome.Console.desktop"
          "bitwarden.desktop"
          "org.gnome.Geary.desktop"
          "org.signal.Signal.desktop"
          "com.github.rssguard.desktop"
        ];
      };
      "org/gnome/shell/keybindings" = {
        switch-to-application-1 = ["<Super>f"]; #firefox
        switch-to-application-2 = ["<Super>c"]; #vscodium
        switch-to-application-3 = ["<Super>t"]; #Terminal
        switch-to-application-4 = ["<Super>b"]; #bitwarden
        switch-to-application-5 = ["<Super>e"]; #Email client geary
        switch-to-application-6 = ["<Super>s"]; #signal
        switch-to-application-7 = ["<Super>r"]; #RSS-Guard
        focus-active-notification = []; #ich will mit Super n den message tray
        toggle-message-tray=["<Super>n"];
        toggle-overview = []; #geht auch allein mit Super
      };
      "org/gnome/desktop/wm/preferences" = {
        num-workspaces = 9;
        workspace-names = [ "Web" "VSCodium" "Terminal" "Bitwarden" "Kommunikation" "Gimp" "News" "8" "9"];
      };
      "org/gnome/shell/extensions/auto-move-windows" = {
        application-list = [
          "org.mozilla.firefox.desktop:1"
          "codium.desktop:2"
          "org.gnome.Console.desktop:3"
          "bitwarden.desktop:4"
          "org.signal.Signal.desktop:5"
          "org.gnome.Geary.desktop:5"
          "gimp.desktop:6"
          "com.github.rssguard.desktop:7"
        ];
      };
      "org/gnome/Weather" = {
        locations=["<(uint32 2, <('Vienna', 'LOWW', true, [(0.83979426423570236, 0.2891428852314914)], [(0.84124869946126679, 0.28565222672750273)])>)>"];
      };
      "org/gnome/shell/weather" = {
        automatic-location = true;
        locations=["<(uint32 2, <('Vienna', 'LOWW', true, [(0.83979426423570236, 0.2891428852314914)], [(0.84124869946126679, 0.28565222672750273)])>)>"];
      };
      "org/gnome/desktop/wm/keybindings" = {
        close = ["<Super>q"];
        cycle-windows = ["<Super>Tab"];
        cycle-windows-backward = ["<Shift><Super>Tab"];
        maximize = [];
        maximize-horizontally = [];
        minimize = ["<Super>Down"];
        unmaximize = [];
        move-to-workspace-1 = ["<Shift><Super>exclam"];
        move-to-workspace-2 = ["<Shift><Super>quotedbl"];
        move-to-workspace-3 = ["<Shift><Super>section"];
        move-to-workspace-4 = ["<Shift><Super>dollar"];
        move-to-workspace-5 = ["<Shift><Super>percent"];
        move-to-workspace-6 = ["<Shift><Super>ampersand"];
        move-to-workspace-7 = ["<Shift><Super>slash"];
        move-to-workspace-8 = ["<Shift><Super>parenleft"];
        move-to-workspace-9 = ["<Shift><Super>parenright"];
        raise-or-lower = ["<Super>h"];
        switch-applications = ["<Alt>Tab"];
        switch-applications-backward = ["<Shift><Alt>Tab"];
        switch-input-source = [];
        switch-input-source-backward = [];
        switch-to-workspace-1 = ["<Super>1"];
        switch-to-workspace-2 = ["<Super>2"];
        switch-to-workspace-3 = ["<Super>3"];
        switch-to-workspace-4 = ["<Super>4"];
        switch-to-workspace-5 = ["<Super>5"];
        switch-to-workspace-6 = ["<Super>6"];
        switch-to-workspace-7 = ["<Super>7"];
        switch-to-workspace-8 = ["<Super>8"];
        switch-to-workspace-9 = ["<Super>9"];
        toggle-fullscreen=["<Super>space"];
        toggle-maximized=["<Super>Return"];
      };
      "org/gnome/settings-daemon/plugins/media-keys" = {
        custom-keybindings= ["/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/"];
        home= ["<Super>d"];
      };
      "org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0" = {
        binding = "<Super>BackSpace";
        command = "/run/current-system/sw/bin/guake-toggle";
        name = "quake";
      };
      "apps/guake/general" = {
        bbreviate-tab-names = false;
        display-n = 0;
        display-tab-names = 0;
        gtk-use-system-default-theme = true;
        hide-tabs-if-one-tab = false;
        history-size = 1000;
        load-guake-yml = true;
        max-tab-name-length = 100;
        mouse-display = true;
        open-tab-cwd = true;
        prompt-on-quit = true;
        restore-tabs-notify = true;
        restore-tabs-startup = true;
        save-tabs-when-changed = true;
        scroll-keystroke = true;
        set-window-title = true;
        start-fullscreen = false;
        tab-ontop = true;
        use-default-font = true;
        use-popup = true;
        use-scrollbar = true;
        use-trayicon = true;
        window-halignment = 0;
        window-height = 62;
        window-losefocus = false;
        window-refocus = false;
        window-tabbar = true;
        window-valignment = 0;
        window-width = 60;
      };
    };

    # Autostart Applikationen
    home.file = {
      ".config/autostart/org.signal.Signal.desktop".source = config.lib.file.mkOutOfStoreSymlink "/var/lib/flatpak/exports/share/applications/org.signal.Signal.desktop";
      ".config/autostart/guake.desktop".source = config.lib.file.mkOutOfStoreSymlink "/run/current-system/sw/share/applications/guake.desktop";
      ".config/autostart/geary-autostart.desktop".source = config.lib.file.mkOutOfStoreSymlink "/run/current-system/sw/share/applications/geary-autostart.desktop";
      ".config/autostart/bitwarden.desktop".source = config.lib.file.mkOutOfStoreSymlink "/run/current-system/sw/share/applications/bitwarden.desktop";
      ".config/autostart/org.gnome.Console.desktop".source = config.lib.file.mkOutOfStoreSymlink "/run/current-system/sw/share/applications/org.gnome.Console.desktop";
      ".config/autostart/com.nextcloud.desktopclient.nextcloud.desktop".source = config.lib.file.mkOutOfStoreSymlink "/run/current-system/sw/share/applications/com.nextcloud.desktopclient.nextcloud.desktop";
      ".config/autostart/ch.protonmail.protonmail-bridge.desktop".source = config.lib.file.mkOutOfStoreSymlink "/var/lib/flatpak/exports/share/applications/ch.protonmail.protonmail-bridge.desktop";
    };
  };
}