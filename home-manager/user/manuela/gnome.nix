{ config, lib, pkgs, ... }:

with lib;

let
  sysconfig = (import <nixpkgs/nixos> {}).config;
in {
  config = mkIf (sysconfig.services.xserver.enable){
    # dconf dump / > ~/Downloads/dconf.dump hilft bei Kontrolle der dconf Einstellungen
    dconf.settings = {
      "org/gnome/shell" = {
        enabled-extensions = [
          "trayIconsReloaded@selfmade.pl"
          "places-menu@gnome-shell-extensions.gcampax.github.com"
          "apps-menu@gnome-shell-extensions.gcampax.github.com"
          ];
        favorite-apps = [
          "org.mozilla.firefox.desktop"
          "org.signal.Signal.desktop"
        ];
      };
      "org/gnome/Weather" = {
        locations=["<(uint32 2, <('Vienna', 'LOWW', true, [(0.83979426423570236, 0.2891428852314914)], [(0.84124869946126679, 0.28565222672750273)])>)>"];
      };
      "org/gnome/shell/weather" = {
        automatic-location = true;
        locations=["<(uint32 2, <('Vienna', 'LOWW', true, [(0.83979426423570236, 0.2891428852314914)], [(0.84124869946126679, 0.28565222672750273)])>)>"];
      };
    };

    # Autostart Applikationen
    home.file = {
      ".config/autostart/com.nextcloud.desktopclient.nextcloud.desktop".source = config.lib.file.mkOutOfStoreSymlink "/run/current-system/sw/share/applications/com.nextcloud.desktopclient.nextcloud.desktop";
    };
  };
}