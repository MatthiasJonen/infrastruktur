{ config, lib, pkgs, ... }:

with lib;

let
  sysconfig = (import <nixpkgs/nixos> {}).config;
in {
  config = mkIf (sysconfig.services.xserver.enable){
    programs.obs-studio.enable = true;
    home.packages = with pkgs; [
      gimp-with-plugins
      exiftool # metadata cleaner, good command for cleaning metadata from all png files in current directory: "exiftool -all= -ext png ."
    ];
  };
}