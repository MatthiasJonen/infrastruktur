{ config, lib, pkgs, ... }:

with lib;

let
  sysconfig = (import <nixpkgs/nixos> {}).config;
in {
  config = mkIf (sysconfig.services.xserver.enable){
    programs.chromium = {
      enable = true;
      extensions = [
        { id = "nngceckbapebfimnlniiiahkandclblb"; } #bitwarden
        { id = "aookogakccicaoigoofnnmeclkignpdk"; } #neeva
      ];
    };
  };
}