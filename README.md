# Infrastruktur
Ziel dieses Repositories ist, eine reproduzierbare IT Infrastruktur für Zuhause zu erreichen.
Dies versuche ich mit einer Mischung aus verschiedenen Tools und Konzepten zu erreichen.
## Tools und Services
Hier liste ich die Tools und Services, die ich für meine Infrastruktur nutze.
- Virtualisierung: Proxmox, Docker, Hetzner
- DNS: hetzner
- Reverse Proxy: caddyserver
- Betriebssystem nixos

## Grundsätzliche Struktur der Nix Konfiguration
Die Systemkonfiguration führe ich mit klassischem NixOS durch. Die Struktur ist wie folgt:
Hosts setzen sich aus Profilen und diese aus Modulen zusammen.
Zusätzlich verwende ich zusätzlich Home-Manager für Userspezifische Konfigurationen.
Grundsätze für Moduldefinition:
 - Userspezifisch aktivierbar
 - Graphische Programme nur bei Workstations aktiv. Hierzu frage ich ab, ob beim Host ```sysconfig.services.xserver.enable = true``` ist.

### Secret Management in der Nix Konfiguration
Das Secret Management mache ich mit [agenix](https://github.com/ryantm/agenix).
Der Einfachheit halber habe ich einen eigenen [Key](#private-key-für-agenix-laden) für agenix definiert, der auf allen Hosts gleich ist.


## Manuelle Wartung der Konfiguration
Für einige Themen habe ich noch keine 100% Automatisation gefunden. Diese sind hier beschrieben.

### Konfiguration weiterentwickeln
Repository mit SSH - Key klonen und ```./install.sh``` starten.
**Achtung:** wenn die Entwicklung abgeschlossen ist, die Verlinkung aufs Entwicklungs-Repo wieder [rückgängig](#git-repository-clonen-und-mit-nixos-konfiguration-verlinken) machen.

### Tailscale authkey erneuern
Der Authorisation Key von Tailscale hat ein Ablaufdatum. Daher muss ich den regelmäßig erneuern:
1. Neuen [Auth Key](https://login.tailscale.com/admin/settings/keys) generieren.
2. Diesen im Secret "authkey-tailscale.age" statt dem alten Auth Key einfügen.

### Displaylink propreitärer Blob - manuelles integrieren in nix store
Das ist meiner Beobachtung nur einmal pro Host, bei dem ich Displaylink nutzen muss, notwendig.
```
cd /root/infrastruktur/modules/laptop
nix-prefetch-url file://$PWD/displaylink-55.zip
```

### Installation eines neuen Hosts
Hier sind die manuellen Schritte beschrieben, die für einen neuen Host notwendig sind.
#### Lokaler Host
Diese Schritte als root durchführen. Ich habe meinen in meinem bitwarden-vault gespeichert.

##### Private Key für agenix laden
```
mkdir /root/.secrets
cd /root/.secrets
nix-shell -p bitwarden-cli
bw login
bw list items --pretty --search infrastruktur
bw get attachment agenix-infrastruktur-key --itemid <id des Eintrags mit agenix-infrastruktur-key als attachement>
chown root:root agenix-infrastruktur-key
chmod 600 agenix-infrastruktur-key
```

##### Git Repository clonen und mit NixOS-Konfiguration verlinken
Diese Schritte als root durchführen.
```
cd /root
git clone https://gitlab.com/MatthiasJonen/infrastruktur.git

cd /root/infrastruktur
./install.sh
```
#### Remote Host bei Hetzner
Der [nixos-infect](https://github.com/elitak/nixos-infect) Beschreibung folgen.
Am remoten Host per ssh einloggen und folgende Schritte durchführen:
1. In /etc/nixos/configuration.nix den Hostname richtig setzen und python und git installieren.
Dabei in der ersten Zeile { ... }: { durch { pkgs,... }: { ersetzen
  ```
    environment.systemPackages = with pkgs; [ python3Full git ];
  ```
2. per ```nixos-rebuild switch``` auf die neue Konfiguration wechseln
3. am Lokalen Host im Infrastruktur Repo ein directory für den neunen Hosts unter hosts anlegen und dorthin navigieren.
4. configuration.nix und hardware-configuration.nix in dieses neue directory kopieren.
5. Änderungen auf gitlab in main mergen

Hilfreiche Statements
```
ssh -i ~/.ssh/hetzner root@<ip des neuen Hosts>
scp -r ~/.ssh/hetzner root@<ip des neuen Hosts>:etc/nixos/ .

```
Ansible - Playbook initialisierung.yaml wie folgt aus directory ansible starten:
```
ansible-playbook --private-key ~/.ssh/hetzner --inventory <ip des neuen Hosts>, --limit <ip des neuen Hosts> --ask-become-pass --ask-vault-password --user root initialisierung.yaml

Danach nochmal per ```nixos-rebuild switch``` auf neue Konfiguration wechseln.
```
